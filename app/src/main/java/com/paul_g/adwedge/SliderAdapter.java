package com.paul_g.adwedge;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SliderAdapter extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;
    public SliderAdapter(Context context){
        this.context=context;
    }

    public int[] slideimages ={
          R.drawable.samsung,
          R.drawable.shoes,
          R.drawable.uganda_money
    };

    public String[] slideheadings ={
           "Get the latest info on favourite brands",
            "Direct refferal to best deals in town",
            "Get paid as you get informed"
    };

    @Override
    public int getCount() {
        return slideheadings.length;
    }


    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == (RelativeLayout) o;
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position){
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slider_layout, container, false);

        ImageView imageView = (ImageView) view.findViewById(R.id.iv_welcome_slider);
        TextView textView =(TextView) view.findViewById(R.id.tv_welcome_headings);
        imageView.setImageResource(slideimages[position]);
        textView.setText(slideheadings[position]);


container.addView(view);


        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }
}
