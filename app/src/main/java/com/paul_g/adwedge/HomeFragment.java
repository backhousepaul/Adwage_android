package com.paul_g.adwedge;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ViewFlipper;


public class HomeFragment extends Fragment {
    String item_clicked;
    DrawerLayout mDrawerLayout;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        final String [] menuItems= {"View Ads","Promotions", "Flash sale", "Account"} ;// This array contains texts of displayed below category images
//       this code displays add categories in a grid view
        int [] menuImages = {R.drawable.adverts,R.drawable.promoz,R.drawable.flash_2,R.drawable.accounts};// this is an array containing images of the add categories
        final GridView gridView = (GridView) view.findViewById(R.id.gridView);
        mDrawerLayout = view.findViewById(R.id.drawer_layout);
        ImageView imageView =(ImageView) view.findViewById(R.id.featured);//this is an image view for the featured ads

        ViewFlipper simpleViewFlipper = view.findViewById(R.id.vf_featured); // initiate a ViewFlipper
        ImageView imageView2 = new ImageView(getActivity());// create a ImageView
        imageView2.setImageResource(R.drawable.freedelivery);// set resource image in ImageView
        simpleViewFlipper.addView(imageView2);// add the ImageView in ViewFlipper
        imageView2.setScaleType(ImageView.ScaleType.FIT_XY);//scale
        simpleViewFlipper.setClipToOutline(true);//this call allows the image to have round corners

        Animation in = AnimationUtils.loadAnimation(getActivity(),android.R.anim.slide_in_left); // load an animation
        simpleViewFlipper.setInAnimation(in); // set in Animation for ViewFlipper

        MainAdapter adapter= new MainAdapter(getActivity(),menuItems,menuImages);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
item_clicked= menuItems[position];
//                Toast.makeText(getActivity().getBaseContext(), item_clicked, Toast.LENGTH_SHORT).show();
    //Test
//                Toast.makeText(getActivity().getBaseContext(), "ItemClicked"+menuItems[position], Toast.LENGTH_LONG).show();
                if(item_clicked == "View Ads"){
                    Intent intent = new Intent( getActivity().getBaseContext(),
                            AddPlayerActivity.class);
                    startActivity(intent);
                }else if (item_clicked == "Promotions"){
                    Toast.makeText(getActivity().getBaseContext(), "ItemClicked"+menuItems[position], Toast.LENGTH_LONG).show();

                }else if (item_clicked == "Account"){
//                     mDrawerLayout.openDrawer(GravityCompat.START);

                }



            }
        });





//        ArrayAdapter<String> arrayAdapter= new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,menuItems);
//gridView.setAdapter(arrayAdapter);
        return view;

    }
}
