package com.paul_g.adwedge;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WelcomeActivity extends AppCompatActivity {
private ViewPager mslideViewPager;
private LinearLayout mDotLayout;
private SliderAdapter sliderAdapter;
private Button mBackBtn, mNextBtn;
private int mCurrentPage;


private TextView [] mDots;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        mslideViewPager = (ViewPager) findViewById(R.id.slider_view_pager);
        mDotLayout = (LinearLayout) findViewById(R.id.dots_layout);
        mBackBtn = (Button) findViewById(R.id.backBtn);
        mNextBtn = (Button) findViewById(R.id.nextBtn);

        sliderAdapter = new SliderAdapter(this);
        mslideViewPager.setAdapter(sliderAdapter);
        addDotIndicator(0);
        mslideViewPager.addOnPageChangeListener(viewListener);
    }

    public void addDotIndicator(int position){
        mDots = new TextView[3];
        mDotLayout.removeAllViews();
        for (int i = 0; i<mDots.length; i++){
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226;"));
            mDots[i].setTextSize(55);
            mDots[i].setTextColor(getResources().getColor(R.color.dot_slider_basic_color));
            mDotLayout.addView(mDots[i]);

        }
        if (mDots.length > 0){
            mDots[position].setTextColor(getResources().getColor(R.color.dot_slider_scroll_color));

        }

    }

    ViewPager.OnPageChangeListener  viewListener= new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
            addDotIndicator(i);
            mCurrentPage = i;
            if (i==2){
                SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("firstStart", false);
                editor.apply();
            }

//            if(i==0){
//                mNextBtn.setEnabled(true);
//                mBackBtn.setEnabled(false);
//                mBackBtn.setVisibility(View.INVISIBLE);
//
//                mNextBtn.setText("Next");
//                mBackBtn.setText("");
//            }else if (i == mDots.length - 1){
//                mNextBtn.setEnabled(true);
//                mBackBtn.setEnabled(true);
//                mBackBtn.setVisibility(View.VISIBLE);
//
//                mNextBtn.setText("Finish");
//                mBackBtn.setText("Back");
//            }
        }


        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };
}