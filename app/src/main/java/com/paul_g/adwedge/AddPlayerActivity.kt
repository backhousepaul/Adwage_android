package com.paul_g.adwedge

import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import android.widget.VideoView
import com.bolaware.viewstimerstory.Momentz
import com.bolaware.viewstimerstory.MomentzCallback
import com.bolaware.viewstimerstory.MomentzView
import com.squareup.picasso.Callback
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_adplayer.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject
import java.net.URL
import java.util.concurrent.Executors


class AddPlayerActivity : AppCompatActivity(), MomentzCallback {


//    var images = arrayOf(
//            "http://content.readymediaafrica.com/adwedge/adds/IMG_20200706_000053.jpg",
//        "https://africa-public.food.jumia.com/marketing/production/ke/images/nl/HP%20BANNER%202019/Discover-all-our-Pizza.jpg",
//        "http://content.readymediaafrica.com/adwedge/adds/IMG-20200705-WA0048.jpg",
//            "http://content.readymediaafrica.com/adwedge/adds/IMG_20200706_000120.jpg",
//            "http://content.readymediaafrica.com/adwedge/adds/IMG_20200706_000749.jpg"
//    )
//    val images_array = Array<String>(5)
    var arraylist=ArrayList<String>()
    var videos = arrayOf("https://images.all-free-download.com/footage_preview/mp4/triumphal_arch_paris_traffic_cars_326.mp4");
    var myvideo = videos[0];
//    var myimage = images[0];
    var myimage = "";
    var x = 0;
//    var images_array: Array<String>? = null
//    fun append(arr: Array<Int?>, element: Int): Array<Int?> {
//        val array = arrayOfNulls<Int>(arr.size + 1)
//        System.arraycopy(arr, 0, array, 0, arr.size)
//        array[arr.size] = element
//        return array
//    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adplayer)

        val executor = Executors.newScheduledThreadPool(5)

        doAsync(executorService = executor){
            val result = URL("https://content.readymediaafrica.com/adwedge/adwage_admin/api/get_ads.php?YO_PHPSSID=1234&status=1").readText()
//            toast(result)
            Log.d(javaClass.simpleName, result)
            uiThread {

//                var id: Int
//                var title: String
                var urls: String
                val array = JSONArray(result)
                for (i in 0 until array.length()) {
                    val row: JSONObject = array.getJSONObject(i)
//                    id = row.getInt("ad_id")
//                    title = row.getString("title")
                    urls = row.getString("media_content")
                    arraylist.add(urls);
//                    toast(urls)
//                    var images = arrayOf( urls)
                }
                // converting arraylist to array
//              images_array= arraylist.toTypedArray()
//                toast(images_array!![1]);
//                myimage = images_array[2];

            }

        }
//    myimage = images_array!![0];
    ;
//    toast(images_array!![0]);
//    myimage = images_array[0];

        // show a textview
//        val textView = TextView(this)
//        textView.text = "Hello, You can display TextViews"
//        textView.textSize = 20f.toPixel(this).toFloat()
//        textView.gravity = Gravity.CENTER
//        textView.setTextColor(Color.parseColor("#ffffff"))

        //show a customView
//        val customView = LayoutInflater.from(this).inflate(R.layout.custom_view, null)



        // show an imageview be loaded from file
//        val locallyLoadedImageView = ImageView(this)
//        locallyLoadedImageView.setImageDrawable(
//            ContextCompat.getDrawable(
//                this,
//                R.drawable.bieber
//            )
//        )

        //image to be loaded from the internet

//        fun main() {
//            println("val internetLoadedImageView = ImageView(this)")
//        }
//        main()
//        val internetLoadedImageView = ImageView(this)
        val internetLoadedImageView2 = ImageView(this)
        val internetLoadedImageView3 = ImageView(this)
        val internetLoadedImageView4 = ImageView(this)

        //video to be loaded from the internet
        val internetLoadedVideo = VideoView(this)
//        val listOfViews = ArrayList<String>()
//        listOfViews.add(MomentzView(textView, 5).toString())
//

        val listOfViews =  listOf(
//                println("MomentzView(internetLoadedImageView, 10)"),
                MomentzView(internetLoadedImageView2, 10),
                MomentzView(internetLoadedImageView3, 10),
                MomentzView(internetLoadedImageView4, 10),
            MomentzView(internetLoadedVideo, 60)
        )



        Momentz(this, listOfViews as List<MomentzView>, container, this).start()
    }


    override fun onNextCalled(view: View, momentz: Momentz, index: Int) {
        val executor = Executors.newScheduledThreadPool(5)

        doAsync(executorService = executor){
            val result = URL("https://content.readymediaafrica.com/adwedge/adwage_admin/api/get_ads.php?YO_PHPSSID=1234&status=1").readText()
//            toast(result)
            Log.d(javaClass.simpleName, result)
            uiThread {

//                var id: Int
//                var title: String
                var urls: String
                val array = JSONArray(result)
                for (i in 0 until array.length()) {
//                    adding url links to array
                    val row: JSONObject = array.getJSONObject(i)
//                    id = row.getInt("ad_id")
//                    title = row.getString("title")
                    urls = row.getString("media_content")
                    arraylist.add(urls);
//                    toast(urls)
//                    var images = arrayOf( urls)
                }
                // converting arraylist to array
               val imagesArray: Array<String> = arraylist.toTypedArray()
//                toast("URL LINK "+ x + imagesArray[0]);
                myimage = imagesArray[x];

                if (view is VideoView) {
                    momentz.pause(true)
                    playVideo(view, index, momentz)
                } else if ((view is ImageView) && (view.drawable == null)) {
                    momentz.pause(true)
                    Picasso.get()
                            .load(myimage)
                            .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                            .into(view, object : Callback {
                                override fun onSuccess() {
                                    momentz.resume()
//                        Toast.makeText(this@AddPlayerActivity, "Image loaded from the internet", Toast.LENGTH_LONG).show()
                                }

                                override fun onError(e: Exception?) {
                                    Toast.makeText(this@AddPlayerActivity,e?.localizedMessage,Toast.LENGTH_LONG).show()
                                    e?.printStackTrace()
                                }
                            })
                    Handler().postDelayed({

                        //Do something after 100ms
                        myimage = imagesArray[x];
//                myimage = images_array[x];
                x++
                    }, 100)
                }

            }

        }

    }

    override fun done() {
        Toast.makeText(this@AddPlayerActivity, "Finished!", Toast.LENGTH_LONG).show()
    }

    private fun playVideo(videoView: VideoView, index: Int, momentz: Momentz) {
        val str = myvideo;
        val uri = Uri.parse(str)

        videoView.setVideoURI(uri)

        videoView.requestFocus()
        videoView.start()

        videoView.setOnInfoListener(object : MediaPlayer.OnInfoListener {
            override fun onInfo(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                    // Here the video starts
                    momentz.editDurationAndResume(index, (videoView.duration) / 1000)
                    Toast.makeText(this@AddPlayerActivity, "Video loaded from the internet", Toast.LENGTH_LONG).show()
                    return true
                }
                return false
            }
        })
    }



}

