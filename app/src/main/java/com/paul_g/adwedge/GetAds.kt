package com.paul_g.adwedge
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject
import java.net.URL
import java.util.concurrent.Executors

class GetAds {
    private var arraylist=ArrayList<String>()
    var images_array: Array<String>? = null
    var myimage = "";

    fun onFetchAds(): Array<String>? {
        val executor = Executors.newScheduledThreadPool(5)
        doAsync(executorService = executor) {

            val result = URL("http://content.readymediaafrica.com/adwedge/get_adds.php").readText()
            uiThread {

//                var id: Int
//                var title: String
                var urls: String
                val array = JSONArray(result)
                for (i in 0 until array.length()) {
                    val row: JSONObject = array.getJSONObject(i)
//                    id = row.getInt("ad_id")
//                    title = row.getString("title")
                    urls = row.getString("media_content")
                    arraylist.add(urls);
//                    toast(urls)
//                    var images = arrayOf( urls)
                }
                // converting arraylist to array
               images_array= arraylist.toTypedArray()
//                toast(images_array[0]);

//                myimage = images_array[0];
            }
        }

return images_array;
    }
}