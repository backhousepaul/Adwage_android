package com.paul_g.adwedge;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity {

    EditText etEmail, etPassword;
    TextView tvRegister, tv_forgot_password;
    Button btnLogin;

    final String url_Login = "https://content.readymediaafrica.com/adwedge/login_user.php";
    final String update_user_status = "https://content.readymediaafrica.com/adwedge/update_user_status.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SharedPreferences pref = getSharedPreferences("prefs", MODE_PRIVATE);
        boolean firstStart =  pref.getBoolean("firstStart", true);

        if (firstStart){
            Intent i = new Intent(LoginActivity.this,
                    WelcomeActivity.class);
            startActivity(i);
        }

        etEmail = (EditText) findViewById(R.id.et_email);
        etPassword = (EditText) findViewById(R.id.et_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        tvRegister = (TextView) findViewById(R.id.tv_register);
        tv_forgot_password = (TextView) findViewById(R.id.tv_forgot_password);

        tv_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this,
                        Password_ResetActivity.class);
                startActivity(i);
            }
        });

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this,
                        RegisterActivity.class);
                startActivity(i);
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Email = etEmail.getText().toString();
                String Password = etPassword.getText().toString();

                new LoginUser().execute(Email, Password);
            }
        });
    }




    public class LoginUser extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... strings) {
            String Email = strings[0];
            String Password = strings[1];

            OkHttpClient okHttpClient = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("user_id", Email)
                    .add("user_password", Password)
                    .build();

            Request request = new Request.Builder()
                    .url(url_Login)
                    .post(formBody)
                    .build();

            Response response = null;
            try{
                response = okHttpClient.newCall(request).execute();
                if(response.isSuccessful()){
                    String result = response.body().string();
                    if(result.equalsIgnoreCase("login")){
                        Intent i = new Intent(LoginActivity.this,
                                DashboardActivity.class);
                        startActivity(i);
                        finish();
                    }else if(result.equalsIgnoreCase("first_login_attempt")){
                        Intent i = new Intent(LoginActivity.this,
                                WelcomeActivity.class);
                        startActivity(i);

                        String status_update_finalURL = update_user_status + "?user_id=" + Email;
                        try {
                            OkHttpClient okHttpClient2 = new OkHttpClient();
                            Request request2 = new Request.Builder()
                                    .url(status_update_finalURL)
                                    .get()
                                    .build();
                            Response response2 = null;

                            try {
                                response2 = okHttpClient2.newCall(request2).execute();
                                if (response2.isSuccessful()) {
//                                    String result2 = response.body().string();
                                    showToast("User status successfully");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }




                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        finish();
                    }else{
                       showToast("Email or Password mismatched!");
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
    }



    public void showToast(final String Text){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(LoginActivity.this,
                        Text, Toast.LENGTH_LONG).show();
            }
        });
    }
}
