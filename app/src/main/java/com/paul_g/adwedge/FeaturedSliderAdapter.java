package com.paul_g.adwedge;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FeaturedSliderAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private String [] menuItems;
    private int[] menuImages;

    public FeaturedSliderAdapter(Context c, String[] menuItems, int[] menuImages) {
        context=c;
        this.menuItems = menuItems;
        this.menuImages = menuImages;
    }


    @Override
    public int getCount() {
        return menuItems.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null){
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.row_item, null);
        }
        ImageView imageView = convertView.findViewById(R.id.iv_featured_item);
//        TextView textView = convertView.findViewById(R.id.tv_grid_view_item);
imageView.setImageResource(menuImages[position]);
//textView.setText(menuItems[position]);
        return convertView;
    }
}
